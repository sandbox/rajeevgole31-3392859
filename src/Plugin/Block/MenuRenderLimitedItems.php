<?php

namespace Drupal\menu_render_limited_items\Plugin\Block;

use Drupal\Core\Form\FormStateInterface;
use Drupal\system\Plugin\Block\SystemMenuBlock;

/**
 * Provides a Menu block to render limited menu items.
 *
 * @Block(
 *   id = "menu_render_limited_items",
 *   admin_label = @Translation("Menu Render Limited Items: Menu"),
 *   category = @Translation("Menu Render Limited Items Block"),
 *   deriver = "Drupal\system\Plugin\Derivative\SystemMenuBlock"
 * )
 */
class MenuRenderLimitedItems extends SystemMenuBlock {

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = parent::build();
    // If the limit is not set or set to zero.
    if (!isset($this->configuration['limit']) || empty($this->configuration['limit'])) {
      // No change.
      return $build;
    }
    // If the limit has reached, don't show the additional links.
    if (count($build['#items']) > $this->configuration['limit']) {
      $build['#items'] = array_slice($build['#items'], 0, $this->configuration['limit']);
    }
    return $build;

  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    // Check if the entered limit is set to a value of 0 or higher.
    $limit = $form_state->getValue('limit');
    if ((!is_numeric($limit)) || ($limit < 0)) {
      $form_state->setErrorByName('limit', $this->t('The item limitation has to be set to 0 or higher.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state): void {
    $this->configuration['limit'] = $form_state->getValue('limit');
    parent::blockSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    $form['limit'] = [
      '#type' => 'textfield',
      '#description' => $this->t('Set the amount of menu items to be displayed in this menu block. The limit is only for level one. Set 0 to display unlimited items.'),
      '#title' => $this->t('Render Menu Items Limit'),
      '#default_value' => $this->configuration['limit'] ?? 0,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    $config = [
      'limit' => 0,
    ];
    return $config + parent::defaultConfiguration();
  }

}
